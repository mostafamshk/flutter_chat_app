// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

import 'package:chatapp/widgets/pickers/user_image_picker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class AuthForm extends StatefulWidget {
  final Function authenticate;
  final bool isLoading;

  const AuthForm({
    Key? key,
    required this.authenticate,
    required this.isLoading,
  }) : super(key: key);

  @override
  State<AuthForm> createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final _auth = FirebaseAuth.instance;
  final _form = GlobalKey<FormState>();

  final FocusNode _userNameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  Map<String, String> _formValues = {};
  File? _pickedImage;

  bool _submittedBefore = false;
  bool _isLoginMode = true;

  void _onFormSubmitted() {
    if (!_submittedBefore) _submittedBefore = true;
    final isValidated = _form.currentState!.validate();
    if (!isValidated) return;
    _form.currentState!.save();

    widget.authenticate(_formValues['email'], _formValues['password'],
        _formValues['username'],_pickedImage, _isLoginMode);
  }

  @override
  void dispose() {
    _userNameFocusNode.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(30),
      elevation: 10,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(25),
          child: Form(
              key: _form,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    _isLoginMode
                        ? "Login to your account"
                        : "Create a new account",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Theme.of(context).colorScheme.primary),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  if (!_isLoginMode)
                    UserImagePickerWidget(onImagePicked: (File image) {
                      setState(() {
                        _pickedImage = image;
                      });
                    }),
                  TextFormField(
                    key: ValueKey("email"),
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      label: Text("Email"),
                    ),
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (value) {
                      FocusScope.of(context).requestFocus(_userNameFocusNode);
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty)
                        return "Email is required!";
                      if (!value.contains("@")) return "Email is not valid!";
                      return null;
                    },
                    onSaved: (newValue) {
                      _formValues = {..._formValues, 'email': newValue!};
                    },
                    onChanged: (value) {
                      if (!_submittedBefore) return;
                      _form.currentState!.validate();
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  if (!_isLoginMode)
                    TextFormField(
                      key: ValueKey("username"),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(label: Text("Username")),
                      focusNode: _userNameFocusNode,
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(_passwordFocusNode);
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty)
                          return "Username is required!";
                        if (value.length < 6) return "Username is too short!";
                        return null;
                      },
                      onSaved: (newValue) {
                        _formValues = {..._formValues, 'username': newValue!};
                      },
                      onChanged: (value) {
                        if (!_submittedBefore) return;
                        _form.currentState!.validate();
                      },
                    ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    key: ValueKey("password"),
                    obscureText: true,
                    decoration: InputDecoration(label: Text("Password")),
                    focusNode: _passwordFocusNode,
                    textInputAction: TextInputAction.done,
                    onFieldSubmitted: (value) {
                      FocusScope.of(context).unfocus();
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty)
                        return "Password is required!";
                      if (value.length < 6) return "Password is too short!";
                      return null;
                    },
                    onSaved: (newValue) {
                      _formValues = {..._formValues, 'password': newValue!};
                    },
                    onChanged: (value) {
                      if (!_submittedBefore) return;
                      _form.currentState!.validate();
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  if (!widget.isLoading)
                    ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.all(15),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadiusDirectional.circular(15))),
                        onPressed: () {
                          _onFormSubmitted();
                        },
                        icon: Icon(_isLoginMode ? Icons.login : Icons.person),
                        label: Text(_isLoginMode ? "Login" : "Sign up")),
                  SizedBox(
                    height: 10,
                  ),
                  if (!widget.isLoading)
                    TextButton(
                        onPressed: () {
                          setState(() {
                            _isLoginMode = !_isLoginMode;
                          });
                        },
                        child: Text(_isLoginMode
                            ? "Don't have an account? Sign up!"
                            : "Already have an account? Login!")),
                  if (widget.isLoading) CircularProgressIndicator()
                ],
              )),
        ),
      ),
    );
  }
}
