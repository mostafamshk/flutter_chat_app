// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class SingleMessageWidget extends StatelessWidget {
  final String text;
  final String userId;
  final String username;
  final String userImageUrl;

  const SingleMessageWidget({
    Key? key,
    required this.text,
    required this.userId,
    required this.username,
    required this.userImageUrl,
  }) : super(key: key);

  bool isMe() {
    return FirebaseAuth.instance.currentUser!.uid == userId;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          isMe() ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (!isMe())
          Padding(
            padding: const EdgeInsets.only(left: 5.0, bottom: 8, right: 5),
            child: CircleAvatar(
              backgroundImage: NetworkImage(userImageUrl),
            ),
          ),
        Container(
          decoration: BoxDecoration(
            color: isMe()
                ? Theme.of(context).colorScheme.primary
                : Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12),
                topRight: Radius.circular(12),
                bottomLeft: !isMe() ? Radius.circular(0) : Radius.circular(12),
                bottomRight: isMe() ? Radius.circular(0) : Radius.circular(12)),
          ),
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          margin: EdgeInsets.symmetric(vertical: 8),
          child: Column(
            crossAxisAlignment:
                isMe() ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              Text(
                isMe() ? "You" : username,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 10,
                  color: isMe()
                      ? Theme.of(context).colorScheme.onPrimary
                      : Theme.of(context).colorScheme.onSecondary,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                text,
                style: TextStyle(
                    color: isMe()
                        ? Theme.of(context).colorScheme.onPrimary
                        : Theme.of(context).colorScheme.onSecondary,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          constraints:
              BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2),
        ),
        if (isMe())
          Padding(
            padding: const EdgeInsets.only(left: 5.0, bottom: 8, right: 5),
            child: CircleAvatar(
              backgroundImage: NetworkImage(userImageUrl),
            ),
          ),
      ],
    );
  }
}
