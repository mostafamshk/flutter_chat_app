import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class NewMessageWidget extends StatefulWidget {
  const NewMessageWidget({super.key});

  @override
  State<NewMessageWidget> createState() => _NewMessageWidgetState();
}

class _NewMessageWidgetState extends State<NewMessageWidget> {
  final _messageController = TextEditingController();
  bool _isLoading = false;
  String _message = '';

  Future<void> onSendMessage() async {
    setState(() {
      _isLoading = true;
    });
    FocusScope.of(context).unfocus();

    try {
      final user = await FirebaseFirestore.instance
          .collection("users")
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .get();
      await FirebaseFirestore.instance.collection("chat").add({
        'text': _message,
        'createdAt': Timestamp.now(),
        'userId': FirebaseAuth.instance.currentUser!.uid,
        'username': user.data()!['userName'],
        'userImageUrl': user.data()!['imageUrl']
      });
      setState(() {
        _message = "";
        _messageController.text = '';
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          Expanded(
              child: TextField(
            controller: _messageController,
            decoration: InputDecoration(
              label: Text("Send a message..."),
            ),
            onChanged: (value) {
              setState(() {
                _message = value;
              });
            },
          )),
          IconButton(
              onPressed: _message.isEmpty ? null : onSendMessage,
              icon: _isLoading
                  ? CircularProgressIndicator()
                  : Icon(
                      Icons.send,
                      color: _message.isEmpty
                          ? Theme.of(context).disabledColor
                          : Theme.of(context).colorScheme.primary,
                    ))
        ],
      ),
    );
  }
}
