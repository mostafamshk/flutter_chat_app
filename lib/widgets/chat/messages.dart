import 'package:chatapp/widgets/chat/single_message.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class MessagesWidget extends StatelessWidget {
  const MessagesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection("chat")
          .orderBy('createdAt', descending: true)
          .snapshots(),
      builder: (context, chatSnapshot) {
        if (chatSnapshot.connectionState == ConnectionState.waiting) {
          return Expanded(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else {
          final chatDocs = chatSnapshot.data!.docs;
          
          print("chatDoc len: ${chatDocs.length}");
          return Expanded(
            child: ListView.builder(
              reverse: true,
              itemCount: chatDocs.length,
              itemBuilder: (context, index) {
                return SingleMessageWidget(
                  text: chatDocs[index]['text'],
                  userId: chatDocs[index]['userId'],
                  username: chatDocs[index]['username'],
                  userImageUrl: chatDocs[index]['userImageUrl']
                );
              },
            ),
          );
        }
      },
    );
  }
}
