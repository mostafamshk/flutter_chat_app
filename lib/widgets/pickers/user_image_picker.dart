// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:image_picker/image_picker.dart';

class UserImagePickerWidget extends StatefulWidget {
  final Function onImagePicked;

  const UserImagePickerWidget({
    Key? key,
    required this.onImagePicked,
  }) : super(key: key);

  @override
  State<UserImagePickerWidget> createState() => _UserImagePickerWidgetState();
}

class _UserImagePickerWidgetState extends State<UserImagePickerWidget> {
  File? _pickedImage;

  Future<void> _onSelectImage(ImageSource imageSource) async {
    Navigator.of(context).pop();
    final selectedImageFile =
        await ImagePicker().pickImage(source: imageSource , imageQuality: 90);
    setState(() {
      _pickedImage = File(selectedImageFile!.path);
    });
    widget.onImagePicked(_pickedImage);
  }

  void _onTrySelectImage() {
    showModalBottomSheet(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12), topRight: Radius.circular(12))),
      context: context,
      builder: (context) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(50),
            onTap: () {
              _onSelectImage(ImageSource.camera);
            },
            child: Padding(
              padding: const EdgeInsets.all(35.0),
              child: Column(
                children: [
                  Icon(
                    Icons.camera_alt,
                    size: 100,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                  Text(
                    "Camera",
                    style: TextStyle(
                      fontSize: 24,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  )
                ],
              ),
            ),
          ),
          InkWell(
            borderRadius: BorderRadius.circular(50),
            onTap: () {
              _onSelectImage(ImageSource.gallery);
            },
            child: Padding(
              padding: const EdgeInsets.all(35.0),
              child: Column(
                children: [
                  Icon(
                    Icons.image,
                    size: 100,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                  Text(
                    "Gallery",
                    style: TextStyle(
                      fontSize: 24,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
          radius: 40,
          child: _pickedImage == null
              ? InkWell(
                  borderRadius: BorderRadius.circular(40),
                  onTap: _onTrySelectImage,
                  child: Icon(
                    Icons.add_a_photo,
                    size: 40,
                    color: Colors.white,
                  ),
                )
              : null,
          backgroundColor: Theme.of(context).colorScheme.secondary,
          backgroundImage:
              _pickedImage != null ? FileImage(_pickedImage!) : null,
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          "Profile Photo",
          style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
