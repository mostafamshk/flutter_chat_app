import 'package:chatapp/widgets/chat/messages.dart';
import 'package:chatapp/widgets/chat/new_message.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatsScreen extends StatelessWidget {
  static const routeName = '/chats-screen';

  ChatsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final firebaseFirestore = FirebaseFirestore.instance;

    return Scaffold(
      appBar: AppBar(
        title: Text("Chats"),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: DropdownButton(
              items: [
                DropdownMenuItem(
                  child: Row(
                    children: [
                      Icon(
                        Icons.exit_to_app,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text("Logout")
                    ],
                  ),
                  value: "Logout",
                )
              ],
              onChanged: (value) {
                FirebaseAuth.instance.signOut();
              },
              icon: Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: Container(
          width: double.infinity,
          padding: EdgeInsets.all(10),
          child: Column(
            children: [MessagesWidget(), NewMessageWidget()],
          )),
    );
  }
}
