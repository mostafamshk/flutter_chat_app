import 'dart:io';

import 'package:chatapp/widgets/auth/auth_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AuthScreen extends StatefulWidget {
  static const routeName = '/auth-screen';

  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool _isLoading = false;

  Future<void> _authenticate(String email, String password, String? username,
      File? image, bool isLoginMode) async {
    try {
      setState(() {
        _isLoading = true;
      });
      if (image == null && !isLoginMode) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Theme.of(context).colorScheme.error,
            duration: Duration(seconds: 3),
            content: Text(
              "Please upload a profile image.",
              style: TextStyle(color: Colors.white),
            )));
        return;
      }
      if (isLoginMode) {
        final authResult = await _auth.signInWithEmailAndPassword(
            email: email.trim(), password: password.trim());
      } else {
        final authResult = await _auth.createUserWithEmailAndPassword(
            email: email.trim(), password: password.trim());
        final storageRef = FirebaseStorage.instance
            .ref()
            .child("user_images")
            .child("${authResult.user!.uid}.jpg");
        await storageRef.putFile(image!).whenComplete(() => null);
        final profileImageUrl = await storageRef.getDownloadURL();
        await FirebaseFirestore.instance
            .collection("users")
            .doc(authResult.user!.uid)
            .set({
          'userName': username!.trim(),
          'email': email.trim(),
          'imageUrl': profileImageUrl
        });
      }
    } on PlatformException catch (exception) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          exception.message!,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Theme.of(context).colorScheme.error,
        duration: Duration(seconds: 3),
      ));
    } on FirebaseAuthException catch (exception) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          exception.message!,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Theme.of(context).colorScheme.error,
        duration: Duration(seconds: 3),
      ));
    } catch (exception) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          exception.toString(),
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Theme.of(context).colorScheme.error,
        duration: Duration(seconds: 3),
      ));
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      // appBar: AppBar(
      //   title: Text("Auth Screen"),
      // ),
      body: Container(
        width: double.infinity,
        child: Center(
          child: AuthForm(
            authenticate: _authenticate,
            isLoading: _isLoading,
          ),
        ),
      ),
    );
  }
}
