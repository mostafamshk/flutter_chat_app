import 'package:chatapp/screens/auth_screen.dart';
import 'package:chatapp/screens/chats_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await FirebaseMessaging.instance.requestPermission();
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, badge: true, sound: true);
  FirebaseMessaging.onMessage.listen(
    (event) {
      print(event.data);
    },
  );
  // FirebaseMessaging.onBackgroundMessage();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
              primarySwatch: Colors.pink,
              accentColor: Colors.purple,
              backgroundColor: Colors.pink),
          buttonTheme: ButtonTheme.of(context).copyWith(
              buttonColor: Colors.pink,
              disabledColor: Colors.grey,
              shape: RoundedRectangleBorder(
                  side: BorderSide.none,
                  borderRadius: BorderRadius.all(Radius.circular(20))))),
      home: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            return snapshot.hasData ? ChatsScreen() : AuthScreen();
          }),
      routes: {
        ChatsScreen.routeName: (ctx) => ChatsScreen(),
        AuthScreen.routeName: (ctx) => AuthScreen(),
      },
    );
  }
}
